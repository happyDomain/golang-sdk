package happydomain

import (
	"bytes"
	"encoding/json"
	"net/url"
	"path"

	model "git.happydns.org/happyDomain/model"
)

type Provider struct {
	model.ProviderCombined
	session *HappyDomainAPI
}

func (h *HappyDomainAPI) NewProvider(p *model.ProviderCombined) *Provider {
	return &Provider{
		session:          h,
		ProviderCombined: *p,
	}
}

func (p *Provider) getURL() *url.URL {
	u := p.session.API
	u.Path = path.Join(u.Path, "providers", p.ProviderCombined.Id.String())

	return &u
}

func (p *Provider) DomainAdd(dn string) (Domain, error) {
	formstream := new(bytes.Buffer)
	err := json.NewEncoder(formstream).Encode(model.DomainMinimal{
		IdProvider: p.Id,
		DomainName: dn,
	})
	if err != nil {
		return nil, err
	}

	u := p.session.API
	u.Path = path.Join(u.Path, "domains")

	resp, err := p.session.DoRequest("POST", &u, formstream)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return p.session.NewDomainFromModel(resp.Body)
}

func (p *Provider) Update() error {
	formstream := new(bytes.Buffer)
	err := json.NewEncoder(formstream).Encode(p)
	if err != nil {
		return err
	}

	resp, err := p.session.DoRequest("PUT", p.getURL(), formstream)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func (p *Provider) Delete() error {
	resp, err := p.session.DoRequest("DELETE", p.getURL(), nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}
