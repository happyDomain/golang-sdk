package happydomain

import (
	"bytes"
	"encoding/json"
	"net/url"
	"path"

	model "git.happydns.org/happyDomain/model"
)

type Service struct {
	session    *HappyDomainAPI
	parent     *Zone
	apiService *model.ServiceCombined
}

func (z *Zone) NewService(svc *model.ServiceCombined) *Service {
	return &Service{
		session:    z.session,
		parent:     z,
		apiService: svc,
	}
}

func (s *Service) getURL() *url.URL {
	u := s.parent.getURL()
	u.Path = path.Join(u.Path, s.apiService.Domain, "services", s.apiService.Id.String())

	return u
}

func (s *Service) Update() error {
	formstream := new(bytes.Buffer)
	err := json.NewEncoder(formstream).Encode(s.apiService)
	if err != nil {
		return err
	}

	resp, err := s.session.DoRequest("PUT", s.getURL(), formstream)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func (s *Service) Delete() error {
	resp, err := s.session.DoRequest("DELETE", s.getURL(), nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}
