package happydomain

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"path"

	"git.happydns.org/happyDomain/api"
	model "git.happydns.org/happyDomain/model"
)

type Domain interface {
	getURL() *url.URL

	GetId() model.Identifier
	GetIdUser() model.Identifier
	GetIdProvider() model.Identifier
	GetDomainName() string
	GetGroup() string
	GetZoneHistoryIDs() []model.Identifier
	GetZoneHistory() ([]model.ZoneMeta, error)

	Update() error
	Delete() error

	CurrentZone() (model.Identifier, error)
	ImportZone() error
	GetZone(model.Identifier) (*Zone, error)
	GetCurrentZone() (*Zone, error)

	NewZone(*model.Zone) *Zone
}

type domainFromAPIDomain struct {
	session *HappyDomainAPI
	api.APIDomain
}

func (d *domainFromAPIDomain) getURL() *url.URL {
	u := d.session.API
	u.Path = path.Join(u.Path, "domains", d.Id.String())

	return &u
}

func (d *domainFromAPIDomain) GetId() model.Identifier {
	return d.Id
}

func (d *domainFromAPIDomain) GetIdUser() model.Identifier {
	return d.IdUser
}

func (d *domainFromAPIDomain) GetIdProvider() model.Identifier {
	return d.IdProvider
}

func (d *domainFromAPIDomain) GetDomainName() string {
	return d.DomainName
}

func (d *domainFromAPIDomain) GetGroup() string {
	return d.Group
}

func (d *domainFromAPIDomain) GetZoneHistoryIDs() (res []model.Identifier) {
	for _, zm := range d.ZoneHistory {
		res = append(res, zm.Id)
	}

	return
}

func (d *domainFromAPIDomain) GetZoneHistory() ([]model.ZoneMeta, error) {
	return d.ZoneHistory, nil
}

func (d *domainFromAPIDomain) Update() error {
	formstream := new(bytes.Buffer)
	err := json.NewEncoder(formstream).Encode(d)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("PUT", d.getURL().String(), formstream)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return handleError(resp)
	}

	return nil
}

func (d *domainFromAPIDomain) Delete() error {
	req, err := http.NewRequest("DELETE", d.getURL().String(), nil)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return handleError(resp)
	}

	return nil
}

func (d *domainFromAPIDomain) CurrentZone() (model.Identifier, error) {
	if len(d.ZoneHistory) == 0 {
		err := d.ImportZone()
		if err != nil {
			return nil, err
		}
	}

	return d.ZoneHistory[0].Id, nil
}

func (d *domainFromAPIDomain) ImportZone() error {
	return nil
}

func (d *domainFromAPIDomain) GetZone(zid model.Identifier) (*Zone, error) {
	return nil, nil
}

func (d *domainFromAPIDomain) GetCurrentZone() (*Zone, error) {
	return nil, nil
}

func (h *HappyDomainAPI) NewDomainsFromAPI(ds []*api.APIDomain) (ret []Domain) {
	for _, dn := range ds {
		ret = append(ret, &domainFromAPIDomain{
			session:   h,
			APIDomain: *dn,
		})
	}
	return
}

type domainFromModelDomain struct {
	session *HappyDomainAPI
	*model.Domain
}

func (d *domainFromModelDomain) getURL() *url.URL {
	u := d.session.API
	u.Path = path.Join(u.Path, "domains", d.Id.String())

	return &u
}
func (d *domainFromModelDomain) GetId() model.Identifier {
	return d.Id
}

func (d *domainFromModelDomain) GetIdUser() model.Identifier {
	return d.IdUser
}

func (d *domainFromModelDomain) GetIdProvider() model.Identifier {
	return d.IdProvider
}

func (d *domainFromModelDomain) GetDomainName() string {
	return d.DomainName
}

func (d *domainFromModelDomain) GetGroup() string {
	return d.Group
}

func (d *domainFromModelDomain) GetZoneHistoryIDs() (res []model.Identifier) {
	return d.ZoneHistory
}

func (d *domainFromModelDomain) GetZoneHistory() ([]model.ZoneMeta, error) {
	var zones []model.ZoneMeta

	for _, _ = range d.ZoneHistory {
		// TODO
	}

	return zones, nil
}

func (d *domainFromModelDomain) Update() error {
	formstream := new(bytes.Buffer)
	err := json.NewEncoder(formstream).Encode(d)
	if err != nil {
		return err
	}

	resp, err := d.session.DoRequest("PUT", d.getURL(), formstream)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func (d *domainFromModelDomain) Delete() error {
	resp, err := d.session.DoRequest("DELETE", d.getURL(), nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func (d *domainFromModelDomain) CurrentZone() (model.Identifier, error) {
	if len(d.ZoneHistory) == 0 {
		err := d.ImportZone()
		if err != nil {
			return nil, err
		}
	}

	return d.ZoneHistory[0], nil
}

func (d *domainFromModelDomain) ImportZone() error {
	return nil
}

func (d *domainFromModelDomain) GetZone(zid model.Identifier) (*Zone, error) {
	return nil, nil
}

func (d *domainFromModelDomain) GetCurrentZone() (*Zone, error) {
	return nil, nil
}

func (h *HappyDomainAPI) NewDomainsFromModel(ds []*model.Domain) (ret []Domain) {
	for _, dn := range ds {
		ret = append(ret, &domainFromModelDomain{
			session: h,
			Domain:  dn,
		})
	}
	return
}

func (h *HappyDomainAPI) NewDomainFromModel(d io.Reader) (Domain, error) {
	var to *model.Domain

	err := json.NewDecoder(d).Decode(&to)
	if err != nil {
		return nil, err
	}

	return &domainFromModelDomain{
		session: h,
		Domain:  to,
	}, nil
}
