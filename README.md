# happyDomain Golang SDK

Golang bindings to interract with happyDomain (user API only, not administrative API).

## Sample usage

```go
package main

import (
	"log"

	"git.happydomain.org/golang-sdk"
)

func main() {
	h := happydomain.DefaultAPI

	err := h.Login("fred@happydomaun.org", "redacted")
	if err != nil {
		log.Fatal(err)
	}

	log.Println(h.DomainList())
}
```
