package happydomain

import (
	"bytes"
	"encoding/json"
	"net/url"
	"path"

	model "git.happydns.org/happyDomain/model"
)

type Zone struct {
	session *HappyDomainAPI
	parent  Domain
	apiZone *model.Zone
}

func (d *domainFromAPIDomain) NewZone(z *model.Zone) *Zone {
	return &Zone{
		session: d.session,
		parent:  d,
		apiZone: z,
	}
}

func (d *domainFromModelDomain) NewZone(z *model.Zone) *Zone {
	return &Zone{
		session: d.session,
		parent:  d,
		apiZone: z,
	}
}

func (z *Zone) getURL() *url.URL {
	u := z.parent.getURL()
	u.Path = path.Join(u.Path, "zone", z.apiZone.Id.String())

	return u
}

func (z *Zone) Update() error {
	formstream := new(bytes.Buffer)
	err := json.NewEncoder(formstream).Encode(z.apiZone)
	if err != nil {
		return err
	}

	resp, err := z.session.DoRequest("PUT", z.getURL(), formstream)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func (z *Zone) Delete() error {
	resp, err := z.session.DoRequest("DELETE", z.getURL(), nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func (z *Zone) AddZoneService(subdomain string, svctype string, svc *Service) error {
	return nil
}

func (z *Zone) ApplyChange() (*model.ZoneMeta, error) {
	return nil, nil
}
